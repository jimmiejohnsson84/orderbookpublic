#ifndef ORDER_H
#define ORDER_H

enum OrderType {OrderBuy, OrderSell};

class Order
{
public:
	Order(unsigned int orderID, OrderType type, unsigned int quantity, float price);
	~Order() {}

	unsigned int		GetQuantity() const;
	float				GetPrice() const;
    unsigned int        GetOrderID() const;

	unsigned int		GetQuantityRemaining() const;
	void				DecreaseQuantiy(unsigned int quantity);

private:
	unsigned int	m_orderID;
	OrderType		m_type;
	unsigned int	m_quantity;
	unsigned int	m_quantityRemaining;
	float			m_price; // Buy at maximum this price / Sell at the lowest at this price
};

#endif