#include "bintree.h"


unsigned int calculateRemaningValue(const std::vector<Order>& values, int startIndex)
{
	unsigned int retValue = 0;
	for(unsigned int i = startIndex; i < values.size(); i++)
	{ 
		retValue += values[i].GetQuantity();
	}
	return retValue;
}
//------------------------------------------------------------------
void SumSetTree::BuildTree(SumSetTree* node, const std::vector<Order>& values, const float buyPrice, unsigned int& currIndex, const unsigned int targetValue, 
							bool& targetValueFound,	std::vector<int>& valuesToUse)
{
	if (values.empty() || values.size() < currIndex || targetValueFound)
	{
		return;
	}

	
	if (values[currIndex].GetPrice() <= buyPrice) // Only create a withValue if the sellers ask price is lower than the buyers buy price.
	{
		node->m_withValue = new SumSetTree;
		node->m_withValue->m_currValue = node->m_currValue + values[currIndex].GetQuantityRemaining();
		node->m_withValue->m_remaning = calculateRemaningValue(values, currIndex + 1);
	}
	else
	{
		node->m_withValue = nullptr;
	}
	

	node->m_withoutValue = new SumSetTree;
	node->m_withoutValue->m_currValue = node->m_currValue;
	node->m_withoutValue->m_remaning = calculateRemaningValue(values, currIndex + 1);

	if (node->m_withValue && node->m_withValue->m_currValue == targetValue) // We are finished
	{
		valuesToUse.push_back(1);
		targetValueFound = true;
		return;
	}
	if (node->m_withValue && node->m_withValue->m_currValue < targetValue && 
		node->m_withValue->m_currValue + node->m_withValue->m_remaning >= targetValue) // Worth to continue on this path
	{
		valuesToUse.push_back(1);
		currIndex++;
		SumSetTree::BuildTree(node->m_withValue, values, buyPrice, currIndex, targetValue, targetValueFound, valuesToUse);
		currIndex--;
	}
	if (!targetValueFound)
	{
		if (node->m_withoutValue->m_currValue < targetValue && 
			node->m_withoutValue->m_currValue + node->m_withoutValue->m_remaning >= targetValue) // Worth to continue on this path
		{
			valuesToUse.push_back(0);
			currIndex++;
			SumSetTree::BuildTree(node->m_withoutValue, values, buyPrice, currIndex, targetValue, targetValueFound, valuesToUse);
			currIndex--;
		}
	}

	if (!targetValueFound)
	{
		if (!valuesToUse.empty())
		{
			valuesToUse.pop_back();
		}
	}
}
//------------------------------------------------------------------
void SumSetTree::DestroyTree(SumSetTree* node)
{
	if(node->m_withValue)
	{
		DestroyTree(node->m_withValue);
	}
	if(node->m_withoutValue)
	{
		DestroyTree(node->m_withoutValue);
	}
	delete node;
}	
//------------------------------------------------------------------
SumSetTree::SumSetTree()
{
	m_withoutValue = nullptr;
	m_withValue = nullptr;

	m_currValue = 0;
	m_remaning = 0;
}
//------------------------------------------------------------------
SumSetTree::~SumSetTree() // Clean up
{
}
