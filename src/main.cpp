#include <iostream>
#include <chrono>
#include <mysqlx/xdevapi.h>

#include "order.h"
#include "orderbook.h"
#include "db/db_api.h"

using namespace std;
using Clock = std::chrono::steady_clock;
using namespace std::chrono;

void CreateRandomOrderBook(Orderbook &orderBook, float minPrice, unsigned int maxPrice)
{
	srand(time(NULL));
    //srand(500);

	for(int i = 0; i < 800; i++)
	//for(int i = 0; i < 1000; i++)
	{
		int buy = rand() % 100; 

		int quantity = 1 + rand() % 5000;
		float price = minPrice + float(rand() % (maxPrice - static_cast<unsigned int>(minPrice)));
		if(buy < 50)
		{			
			orderBook.AddBuyOrder(Order(1, OrderBuy, quantity, price));
		}
		else
		{
			orderBook.AddSellOrder(Order(1, OrderSell, quantity, price));
		}		
	}
}

void CreateOrderBookTest(Orderbook &orderBook)
{
    orderBook.AddBuyOrder(Order(1, OrderBuy, 10, 5.0f));

    // Simple example (sum set tree.png in videos)
    orderBook.AddSellOrder(Order(1, OrderSell, 10, 7.0f));
    orderBook.AddSellOrder(Order(1, OrderSell, 11, 5.0f));
    orderBook.AddSellOrder(Order(1, OrderSell, 5, 5.0f));
    orderBook.AddSellOrder(Order(1, OrderSell, 3, 5.0f));
    orderBook.AddSellOrder(Order(1, OrderSell, 2, 5.0f));

    // Illustrates back tracking (we need to use the sell orders of 4 and 6 but we explored inlcuding 5 first,
    // show why we need to pop from values to use list and keep track of current index)
    /*
    orderBook.AddSellOrder(Order(1, OrderSell, 10, 7.0f));        
    orderBook.AddSellOrder(Order(1, OrderSell, 6, 5.0f));    
    orderBook.AddSellOrder(Order(1, OrderSell, 4, 5.0f));    
    orderBook.AddSellOrder(Order(1, OrderSell, 5, 5.0f));
    */
}


int main()
{
    // Main program that talks to database
    database::DatabaseConnection db;

    auto timeStart = Clock::now();

    while(true)
    {        
        auto timeNow = Clock::now();

        std::chrono::duration<double> ms = timeNow - timeStart;

        if(ms.count() >= 0.5) // Pull DB once every half second
        {
            timeStart = Clock::now();

            //cout << "Pulling database..." << endl;
            if(db.ReadShouldExecuteOrders())
            {
                cout << "Executing buy orders..." << endl;
                Orderbook orderBook;
                
                db.ReadBuyOrders(orderBook);
                db.ReadSellOrders(orderBook);
                
                orderBook.PrintOrderBook();

                orderBook.ExecuteOrdersAllOrNone();

                cout << "Updating database..." << endl;
                orderBook.PersistExecutedOrdersToDatabase(db);

                orderBook.PrintOrderBook();

                db.ResetExecuteOrders();
            }
        }
    }
    // -------------------------------


    // For local testing
    /*
    Orderbook orderBook;
    CreateOrderBookTest(orderBook);
    orderBook.PrintOrderBook();
    orderBook.ExecuteOrdersAllOrNone();
    */
    
    // Test larger randomized orderbooks
    /*
    Orderbook orderBook;
    CreateRandomOrderBook(orderBook, 1.0f, 50);
    orderBook.ExecuteOrdersAllOrNone();
    */


	return 0;
}