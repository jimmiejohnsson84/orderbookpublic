#ifndef ORDERBOOK_H
#define ORDERBOOK_H

#include "order.h"
#include <vector>
#include <unordered_map>


// Fwds
namespace database
{
    class DatabaseConnection;
}

/*
*	Stores buy and sell prices for security/instrument on a market
*/
class Orderbook
{
public:
	Orderbook();
	~Orderbook() {}

	void		AddBuyOrder(Order order);
	void		AddSellOrder(Order order); // Linear in time, adds the price so that m_sellOrders is sorted

	void		ExecuteOrdersAllOrNone();

    void        PersistExecutedOrdersToDatabase(database::DatabaseConnection &db);

	void		PrintOrderBook();

private:

    const std::vector<unsigned int> &GetDeleteSellOrders() const;
    const std::vector<unsigned int> &GetDeleteBuyOrders() const;


    unsigned int    FindSellOrderIndex(float price) const; // Returns the first index in m_sellOrders with a price < in parameter price

    // Store sell orders sorted
	std::vector<Order>      m_sellOrders;
    std::vector<Order>      m_buyOrders;

    std::vector<unsigned int>        m_deleteSellOrders;
    std::vector<unsigned int>        m_deleteBuyOrders;

	
};

#endif