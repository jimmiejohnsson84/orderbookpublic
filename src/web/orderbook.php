<html>
<head>
    <title>Fish market</title>
</head>
<style>
body {
  background-color: #ffbf4f;
  background-image: url('imgs/bkg_orderbook.png');
}

.middleDiv
{
    background-color: #ffbf4f;
    margin-left: 280px;
    margin-right: 280px;
    border-style: solid;
    border-color: black;
}

p {
    display: flex;
    justify-content: center;
    color:black;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 140%;
    }    
    
h1 {
    margin-left: 280px;
    margin-right: 280px;    
    color:black
    }

h2 {
    margin-left: 280px;
    margin-right: 280px;    
    color:black
    }
    
    
iframe {
    margin-left: 280px;
    margin-right: 280px;
    }
    
ul {
    margin-left: 280px;
    margin-right: 280px;
    color:black;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 140%;
    } 
    
ol {
    margin-left: 280px;
    margin-right: 280px;
    color:black;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 140%;
    } 

.code {
    margin-left: 280px;
    margin-right: 280px;
}
  
.images {
    display: flex;
    justify-content: center;
}
.formNewPost {
    display: flex;
    justify-content: center;
    border-style: solid;
    border-color: black;

    margin-left: 500px;
    margin-right: 500px;

}
 
.leaderBoardTable {
    display: flex;
    justify-content: center;
}

.headlinks {
    margin-left: 280px;
    margin-right: 280px;
}
  
 .styled-table {
    border-collapse: collapse;
    margin: 25px 0;
    font-size: 0.9em;
    font-family: sans-serif;
    min-width: 400px;
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}
.styled-table thead tr {
    background-color: #ffb738;
    color: #000000;
    text-align: left;
}

.styled-table th,
.styled-table td {
    padding: 12px 15px;
}
.styled-table tbody tr {
    border-bottom: 1px solid #ffb738;
}

.styled-table tbody tr:nth-of-type(even) {
    background-color: #f3f3f3;
}

.styled-table tbody tr:last-of-type {
    border-bottom: 2px solid #ffb738;
}
.styled-table tbody tr.active-row {
    font-weight: bold;
    color: #009879;
}
</style>
<body>

<?php
// define variables and set to empty values
$type = $quantity = $price = $comment = $website = $executeorders = $clearorders = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
  $type = test_input($_POST["type"]);
  $quantity = test_input($_POST["quantity"]);
  $price = test_input($_POST["price"]);
  $executeorders = test_input($_POST["executeorders"]);
  $clearorders = test_input($_POST["clearorders"]);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?> 

<div class="middleDiv">

    <div class="images">
    <img src="/imgs/fish_market.gif" alt="Fish market">
    </div>

    <p>Submit an order</p>

    <div class="formNewPost">
        <form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
        <label for="type">Buy/Sell:</label><br>
        <select name = "type">
                <option value = "Buy" selected>Buy</option>
                <option value = "Sell">Sell</option>
            </select><br>

        <label for="quantity">Quantity:</label><br>
        <input type="number" name="quantity" min="1" max="1000000"><br>

        <label for="price">Price:</label><br>
        <input type="number" name="price" min="1" max="1000000"><br>

        <input type="submit" name="submit" value="Submit">  
        </form>
    </div>

    <p>&nbsp;</p>
    <div class="formNewPost">
        <form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
        <input type="checkbox" id="executeorders" name="executeorders" value="execute">
        <label for="executeorders">Match buy and sell orders</label><br>

        <input type="submit" name="submit" value="Submit">  
        </form>
    </div>


    <p>&nbsp;</p>
    <div class="formNewPost">
        <form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
        <input type="checkbox" id="clearorders" name="clearorders" value="clearorders">
        <label for="executeorders">Clear all trades</label><br>

        <input type="submit" name="submit" value="Submit">  
        </form>
    </div>

<?php
// Add an order
$servername = "localhost";
$username = "orderuser";
$password = "missdaisy";
$dbname = "orderbook";

if(strlen($type) > 0  && strlen($quantity) > 0 && strlen($price) > 0)
{ 
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) 
    {
        die("Connection failed: " . $conn->connect_error);
    }

    // Read out the highest ID and just add one to it
    $sqlHighestID = "SELECT ID FROM Orders ORDER BY ID DESC LIMIT 1";
    $result = $conn->query($sqlHighestID);

    $ID = 0;
    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();
      $ID = (int)$row["ID"];
      $ID = $ID + 1;
    }
    if($ID >= 0)
    {
        $typeAsInt = 0;
        
        if($type == "Sell")
        {
            $typeAsInt = 1;
        }

        $sql = "INSERT INTO Orders (ID, Type, Quantity, Price)
                VALUES (" .$ID. ", " .$typeAsInt. ", " .$quantity. ", " .$price. ")";

        #echo $sql;
        if ($conn->query($sql) === TRUE) 
        {
            echo "<p>Order inserted</p>";
        }
        else 
        {
            //echo "Error: " . $sql . "<br>" . $conn->error;
            echo "Ooops, something went wrong!";
        }
    }
    else
    {
        echo "Ooops, something went wrong!";
    }

    $conn->close();
}
else if(strlen($executeorders) > 0)
{
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) 
    {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO ExecuteOrders (Execute) VALUES (1)";
    if ($conn->query($sql) === TRUE) 
    {
        echo "<p>Executing orders</p>";
    }
    else 
    {
        //echo "Error: " . $sql . "<br>" . $conn->error;
        echo "Ooops, something went wrong!";
    }
    $conn->close();
    
}
else if(strlen($clearorders) > 0)
{
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) 
    {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "DELETE FROM Orders";
    if ($conn->query($sql) === TRUE) 
    {
        echo "<p>Clearing all orders</p>";
    }
    else 
    {
        //echo "Error: " . $sql . "<br>" . $conn->error;
        echo "Ooops, something went wrong!";
    }
    $conn->close();    
}

?>

    <p>Buy orders & Sell orders</p>
    <div class="leaderBoardTable">    
    <table class="styled-table">
    <thead>
    <tr>
        <th>Quantity</th>
        <th>Price</th>	
    </tr>
    </thread>
    <tbody>
        <tr>
            <?php
            $servername = "localhost";
            $username = "orderuser";
            $password = "missdaisy";
            $dbname = "orderbook";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) 
            {
            die("Connection failed: " . $conn->connect_error);
            }

            #$sql = "SELECT Type, Quantity, Price FROM Orders WHERE TYPE = 0 ORDER BY Price DESC";
            $sql = "SELECT Type, SUM(Quantity), Price FROM Orders WHERE TYPE = 0 GROUP BY Price ORDER BY Price DESC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) 
            {
            // output data of each row	  
            while($row = $result->fetch_assoc()) 
            {
                echo "<tr>";
                echo "<td>" . $row["SUM(Quantity)"]. "</td> <td>" . $row["Price"] . "</td>";
                echo "</tr>";
            }
            }
            $conn->close();
            ?>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
    
    <table class="styled-table">
    <thead>
    <tr>
        <th>Quantity</th>
        <th>Price</th>	
    </tr>
    </thread>
    <tbody>
        <tr>
            <?php
            $servername = "localhost";
            $username = "orderuser";
            $password = "missdaisy";
            $dbname = "orderbook";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) 
            {
            die("Connection failed: " . $conn->connect_error);
            }

            #$sql = "SELECT Type, Quantity, Price FROM Orders WHERE TYPE = 1 ORDER BY Price DESC";
            $sql = "SELECT Type, SUM(Quantity), Price FROM Orders WHERE TYPE = 1 GROUP BY Price ORDER BY Price DESC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) 
            {
            // output data of each row	  
            while($row = $result->fetch_assoc()) 
            {
                echo "<tr>";
                echo "<td>" . $row["SUM(Quantity)"]. "</td> <td>" . $row["Price"] . "</td>";
                echo "</tr>";
            }
            }
            $conn->close();
            ?>
            </tr>
        </tbody>
    </table> 
    </div>

</div>

 </body>
</html>