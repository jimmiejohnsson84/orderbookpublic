#include "order.h"

Order::Order(unsigned int orderID, OrderType type, unsigned int quantity, float price)
{
	m_orderID = orderID;
	m_type = type;
	m_quantity = quantity;
	m_quantityRemaining = m_quantity;
	m_price = price;
}
//------------------------------------------------------------------
unsigned int Order::GetOrderID() const
{
    return m_orderID;
}
//------------------------------------------------------------------
unsigned int Order::GetQuantity() const
{
	return m_quantity;
}
//------------------------------------------------------------------
unsigned int Order::GetQuantityRemaining() const
{
	return m_quantityRemaining;
}
//------------------------------------------------------------------
void Order::DecreaseQuantiy(unsigned int quantity)
{
	if (m_quantityRemaining >= quantity)
	{
		m_quantityRemaining -= quantity;
	}
}
//------------------------------------------------------------------
float Order::GetPrice() const
{
	return m_price;
}
//------------------------------------------------------------------
