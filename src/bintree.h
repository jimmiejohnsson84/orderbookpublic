#ifndef BINTREE_H
#define BINTREE_H
#include <vector>
#include "order.h"

class SumSetTree
{
public:
	SumSetTree();
	~SumSetTree();

	static void		BuildTree(SumSetTree* node, const std::vector<Order>& values, const float price, unsigned int& currIndex, const unsigned int targetValue, bool& targetValueFound,
								std::vector<int>& valuesToUse);

	static void		DestroyTree(SumSetTree* node);

private:
	
	SumSetTree*		m_withValue;
	SumSetTree*		m_withoutValue;

	unsigned int	m_currValue;
	unsigned int	m_remaning;
};

#endif