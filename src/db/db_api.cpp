#include <iostream>
#include "../order.h"
#include "db_api.h"
#include "../orderbook.h"


using namespace database;
using namespace ::mysqlx;
using namespace std;

#define CATCH_DB_ERROR_AND_PRINT_TO_TERMINAL catch (const mysqlx::Error &err)\
                                            {\
                                                cout << "ERROR: " << err << endl;\
                                            }\
                                            catch (std::exception &ex)\
                                            {\
                                                cout << "STD EXCEPTION: " << ex.what() << endl;\
                                            }\
                                            catch (const char *ex)\
                                            {\
                                                cout << "EXCEPTION: " << ex << endl;\
                                            }


DatabaseConnection::DatabaseConnection() try : m_session(SessionOption::HOST, "localhost",
                    SessionOption::PORT, 33060,
                    SessionOption::USER, "orderuser",
                    SessionOption::PWD, "missdaisy")                    
{    
}
CATCH_DB_ERROR_AND_PRINT_TO_TERMINAL

void DatabaseConnection::ReadBuyOrders(Orderbook &orderbook)
{
    try
    {
        Schema schema = m_session.getSchema("orderbook");
        Table orderTable = schema.getTable("Orders");
        RowResult  orderResult  = orderTable.select("ID", "Type", "Quantity", "Price").
                                    where("Type = 0").execute();

        std::list<Row> rows = orderResult.fetchAll();

        for(Row row : rows)
        {
            Order theOrder(row[0], OrderType::OrderBuy, row[2], row[3]);
            orderbook.AddBuyOrder(theOrder);

            /*
            cout << "Buy: " << endl;
            cout << "     ID: "     << row[0] << endl;
            cout << "    Type: "    << row[1] << endl;
            cout << "Quantity: "    << row[2] << endl;
            cout << "Price: "      << row[3] << endl;
            */
        }


    }
    CATCH_DB_ERROR_AND_PRINT_TO_TERMINAL
}

void DatabaseConnection::ReadSellOrders(Orderbook &orderbook)
{
    try
    {
        Schema schema = m_session.getSchema("orderbook");
        Table orderTable = schema.getTable("Orders");
        RowResult  orderResult  = orderTable.select("ID", "Type", "Quantity", "Price").
                                    where("Type = 1").execute();

        std::list<Row> rows = orderResult.fetchAll();

        for(Row row : rows)
        {
            Order theOrder(row[0], OrderType::OrderSell, row[2], row[3]);
            orderbook.AddSellOrder(theOrder);

            /*
            cout << "Sell: " << endl;
            cout << "     ID: "     << row[0] << endl;
            cout << "    Type: "    << row[1] << endl;
            cout << "Quantity: "    << row[2] << endl;
            cout << "Price: "      << row[3] << endl;
            */
        }


    }
    CATCH_DB_ERROR_AND_PRINT_TO_TERMINAL
}

bool DatabaseConnection::ReadShouldExecuteOrders()
{
    try
    {
        Schema schema = m_session.getSchema("orderbook");
        Table orderTable = schema.getTable("ExecuteOrders");
        RowResult  orderResult  = orderTable.select("Execute").execute();

        std::list<Row> rows = orderResult.fetchAll();

        return !rows.empty();
    }
    CATCH_DB_ERROR_AND_PRINT_TO_TERMINAL

    return false;
}

void DatabaseConnection::DeleteOrders(const std::vector<unsigned int> &orderIDs)
{
    try
    {
        Schema schema = m_session.getSchema("orderbook");
        Table orderTable = schema.getTable("Orders");

        for(auto orderID : orderIDs)
        {
            std::string whereClause = "ID = " + to_string(orderID);
            cout << "where: " << whereClause << endl;
            orderTable.remove().where(whereClause).execute();
        }
    }
    CATCH_DB_ERROR_AND_PRINT_TO_TERMINAL
}

void DatabaseConnection::ResetExecuteOrders()
{
    try
    {
        Schema schema = m_session.getSchema("orderbook");
        Table orderTable = schema.getTable("ExecuteOrders");

        orderTable.remove().execute(); // Drop all (the idea is that there is only one record)
    }
    CATCH_DB_ERROR_AND_PRINT_TO_TERMINAL    
}