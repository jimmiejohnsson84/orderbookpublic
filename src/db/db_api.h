#ifndef DB_API_H
#define DB_API_H

#include <vector>
#include <mysqlx/xdevapi.h>

// Fwds
class Orderbook;

namespace database
{
    class DatabaseConnection
    {
        public:
        DatabaseConnection();
        ~DatabaseConnection() { }

        void ReadBuyOrders(Orderbook &orderbook);
        void ReadSellOrders(Orderbook &orderbook);

        bool ReadShouldExecuteOrders();

        void DeleteOrders(const std::vector<unsigned int> &orderIDs);
        void ResetExecuteOrders();

        private:

        mysqlx::Session     m_session;
    };
}

#endif