#include <iostream>
#include <algorithm>
#include "orderbook.h"
#include "bintree.h"
#include "db/db_api.h"

using namespace std;

Orderbook::Orderbook()
{
}
//------------------------------------------------------------------
void Orderbook::AddBuyOrder(Order order)
{

	m_buyOrders.push_back(order);
}
//------------------------------------------------------------------
unsigned int Orderbook::FindSellOrderIndex(float price) const
{
    // Use binary search to aid finding the spot for insertion
    long mid = 0;
    long prevLow = 0;
    long low = 0;
    long high = m_sellOrders.size() - 1;
	// Repeat until the pointers low and high meet each other
    if(!m_sellOrders.empty())
    {
        while (low <= high) 
        {
            mid = low + (high - low) / 2;

            if (m_sellOrders[mid].GetPrice() > price)
            {
                prevLow = low;
                low = mid + 1;
            }
            else
            {
                high = mid - 1;
            }
        }
    }

    return static_cast<unsigned int>(prevLow);
}
//------------------------------------------------------------------
void Orderbook::AddSellOrder(Order order)
{
    // Use binary search to aid finding the spot for insertion
    int i = FindSellOrderIndex(order.GetPrice());
    
    bool foundSpot = false;
    while(i < m_sellOrders.size() && !foundSpot)
    {
        if(m_sellOrders[i].GetPrice() > order.GetPrice())
        {
            i++;
        }
        else
        {
            foundSpot = true;
        }        
    }
    m_sellOrders.insert(m_sellOrders.begin() + i, order);
}
//------------------------------------------------------------------
const std::vector<unsigned int> &Orderbook::GetDeleteSellOrders() const
{
    return m_deleteSellOrders;
}
//------------------------------------------------------------------
const std::vector<unsigned int> &Orderbook::GetDeleteBuyOrders() const
{
    return m_deleteBuyOrders;
}
//------------------------------------------------------------------
void Orderbook::PrintOrderBook()
{
	cout << "Buy orders: " << endl;
	for (const auto buyOrder : m_buyOrders)
	{
		cout << buyOrder.GetQuantityRemaining() << ", " << buyOrder.GetPrice() << endl;
	}
	cout << "----------------" << endl;
	cout << "Sell orders: " << endl;
	for (const auto sellOrder : m_sellOrders)
	{
		cout << sellOrder.GetQuantityRemaining() << ", " << sellOrder.GetPrice() << endl;
	}
	cout << "----------------" << endl;

}
//------------------------------------------------------------------
void Orderbook::ExecuteOrdersAllOrNone()
{
	unsigned int buyOrderIndex = 0;
	for(auto &orderBuy : m_buyOrders)
	{
        // Executes as many buy orders as possible that can be fully filled by existing sale orders
        SumSetTree* BuySell = new SumSetTree();

		//unsigned int index = 0;
        unsigned int index = FindSellOrderIndex(orderBuy.GetPrice());        
        bool foundSpot = false;
        while(index < m_sellOrders.size() && !foundSpot)
        {
            if(m_sellOrders[index].GetPrice() > orderBuy.GetPrice())
            {
                index++;
            }
            else
            {
                foundSpot = true;
            }
        }
        unsigned int startIndex = index;        


		bool targetFound = false;
		std::vector<int> sellOrdersToExecute;

        SumSetTree::BuildTree(BuySell, m_sellOrders, orderBuy.GetPrice(), index, orderBuy.GetQuantity(), targetFound, sellOrdersToExecute);

        if(targetFound)
        {
            cout << "Buy order (" << orderBuy.GetOrderID() << ", " << orderBuy.GetPrice() << ", " << orderBuy.GetQuantity() << ") executed" << endl;
            orderBuy.DecreaseQuantiy(orderBuy.GetQuantity());

            index = 0;
            for(auto executeThisSellOrder : sellOrdersToExecute)
            {
                if(executeThisSellOrder)
                {
                    cout << "Sell order (" << m_sellOrders[startIndex + index].GetOrderID() << ", " << m_sellOrders[startIndex + index].GetPrice() << ", " << m_sellOrders[startIndex + index].GetQuantity() << ") executed" << endl;
                    m_sellOrders[startIndex + index].DecreaseQuantiy(m_sellOrders[startIndex + index].GetQuantity());
                }
                index++;
            }

            // Clean up sell orders that have been cleared
            unsigned int i = 0;
            while(i < m_sellOrders.size())
            {
                if(m_sellOrders[i].GetQuantityRemaining() == 0)
                {
                    cout << "Deleted sell order (" << m_sellOrders[i].GetOrderID() << ", " << m_sellOrders[i].GetPrice() << ", " << m_sellOrders[i].GetQuantity() << ")" << endl;
                    m_deleteSellOrders.push_back(m_sellOrders[i].GetOrderID());

                    m_sellOrders.erase(m_sellOrders.begin() + i);                    
                    i = 0;
                }
                else
                {
                    i++;
                }
                
            }
        }

		SumSetTree::DestroyTree(BuySell);
		buyOrderIndex++;
	}

    // Clean up buy orders that have been cleared
    unsigned int i = 0;
    while(i < m_buyOrders.size())
    {
        if(m_buyOrders[i].GetQuantityRemaining() == 0)
        {
            cout << "Deleted buy order (" << m_buyOrders[i].GetOrderID() << ", " << m_buyOrders[i].GetPrice() << ", " << m_buyOrders[i].GetQuantity() << ")" << endl;
            m_deleteBuyOrders.push_back(m_buyOrders[i].GetOrderID());
            
            m_buyOrders.erase(m_buyOrders.begin() + i);

            i = 0;
        }
        else
        {
            i++;
        }    
    }
}
//------------------------------------------------------------------
void Orderbook::PersistExecutedOrdersToDatabase(database::DatabaseConnection &db)
{
    db.DeleteOrders(GetDeleteSellOrders());
    db.DeleteOrders(GetDeleteBuyOrders());
}
